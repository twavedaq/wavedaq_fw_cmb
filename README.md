# General Information

## Maintainers
Stefan Ritt [stefan.ritt@psi.ch]

## Authors
Stefan Ritt [stefan.ritt@psi.ch]

# Origin
This repository was originally part of a larger repository that is now available here:
https://bitbucket.org/twavedaq/wavedaq_old.git

Note that the major part of the history was preserved when splitting the repository.

# Description
This repositry contains software source files of the Crate Management Board (CMB) for WaveDAQ.

There is a main repository containing all WaveDAQ repositories as submodules:
https://bitbucket.org/twavedaq/wavedaq_main.git
