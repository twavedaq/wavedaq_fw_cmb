/********************************************************************\

  Name:         cmb.c
  Created by:   Stefan Ritt

  Contents:     Firmware for WaveDREAM Crate Management Board using
                a SiLabs C8051F120 CPU

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "cmb.h"

sbit CS8900A_RESET = P0 ^ 5;
sbit RS485_ENABLE = RS485_EN_PIN;

bit submaster_configured;
bit client_configured;
bit flash_client;                // used for EEPROM flashing
bit flash_program;               // used for firmware upgrade

unsigned char _unlocked;         // global flag for write locking

USER_DATA user_data;
CLI_CFG cli_cfg;

MSCB_INFO_VAR code vars[] = {
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "Power",    &user_data.power           }, //  0
   { 1,  UNIT_PERCENT,         0, 0,           0, "Fans",     &user_data.fans            }, //  1
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "HVon",     &user_data.hv_on           }, //  2

   { 1,  UNIT_BYTE,            0, 0,           0, "Error",    &user_data.error           }, //  3

   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U3.3",     &user_data.voltage[0]      }, //  4
   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U5.0",     &user_data.voltage[1]      }, //  5
   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U12",      &user_data.voltage[2]      }, //  6
   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U24",      &user_data.voltage[3]      }, //  7
   { 4,  UNIT_AMPERE,          0, 0, MSCBF_FLOAT, "I",        &user_data.current         }, //  8

   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "HVdemand", &user_data.hv_demand       }, //  9
   { 4,  UNIT_VOLT,            0, 0, MSCBF_FLOAT, "HVmeas",   &user_data.hv_measured     }, // 10
   { 4,  UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "HVcurr",   &user_data.hv_current      }, // 11

   { 4,  UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T0",       &user_data.temp[0]         }, // 12
   { 4,  UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T1",       &user_data.temp[1]         }, // 13
   { 4,  UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T2",       &user_data.temp[2]         }, // 14
   { 4,  UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T3",       &user_data.temp[3]         }, // 15
   { 4,  UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "T_OFF",    &user_data.t_off           }, // 16
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD0",      &user_data.board_power[0]  }, // 17
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD1",      &user_data.board_power[1]  }, // 18
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD2",      &user_data.board_power[2]  }, // 19
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD3",      &user_data.board_power[3]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD4",      &user_data.board_power[4]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD5",      &user_data.board_power[5]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD6",      &user_data.board_power[6]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD7",      &user_data.board_power[7]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD8",      &user_data.board_power[8]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD9",      &user_data.board_power[9]  },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD10",     &user_data.board_power[10] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD11",     &user_data.board_power[11] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD12",     &user_data.board_power[12] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD13",     &user_data.board_power[13] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD14",     &user_data.board_power[14] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "WD15",     &user_data.board_power[15] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "DCB",      &user_data.board_power[16] },
   { 1,  UNIT_BOOLEAN,         0, 0,           0, "TCB",      &user_data.board_power[17] }, // 34

   { 1,  UNIT_ASCII,           0, 0, MSCBF_DATALESS, "SPI",   0                          }, // 35
   { 1,  UNIT_BYTE,            0, 0,           0, "SPI_SLT",  &user_data.spi_slot        }, // 36

   { 1,  UNIT_BYTE,            0, 0,           0, "Unlocked", &user_data.unlocked        }, // 37

   { 0 }
};

MSCB_INFO_VAR *variables = vars;
unsigned char xdata update_data[64];
unsigned long xdata start_time;

unsigned char xdata spi_tx_buf[64];
unsigned char xdata spi_tx_n;

#define SPI_RX_SIZE 64
unsigned char xdata spi_rx_buf[SPI_RX_SIZE];
unsigned char xdata spi_rx_wp;
unsigned char xdata spi_rx_rp;

void user_write(unsigned char index, unsigned char *buf) reentrant;
void user_update(void);

/*------------------------------------------------------------------*/

void setup(void)
{
   unsigned short i;            // software timer

   watchdog_disable();          // Disable watchdog timer

   SFRPAGE = CONFIG_PAGE;       // set SFR page

   XBR0 = 0x04;                 // Enable UART0
   XBR1 = 0x00;
   XBR2 = 0x40;

   // all pins used by the external memory interface are in push-pull mode
   P0MDOUT = 0xFF;
   P1MDOUT = 0xFF;
   P2MDOUT = 0xFF;
   P3MDOUT = 0xFF;
   P5MDOUT = 0xFE;              // P5.0 (DI) is input
   P6MDOUT = 0x7F;              // P6.7 (BACKPLANE_DI) is input

   P0 = 0xE0;                   // /WR, /RD, are high, RESET is high
   P1 = 0x00;
   P2 = 0x00;                   // P1, P2 contain the address lines
   P3 = 0xFF;                   // P3 contains the data lines
   P5 = 0xFF;
   P6 = 0xFF;

   OSCICN = 0x83;               // set internal oscillator to run
                                // at its maximum frequency

   CLKSEL = 0x00;               // Select the internal osc. as
                                // the SYSCLK source

   //Turn on the PLL and increase the system clock by a factor of M/N = 2
   PLL0CN = 0x00;               // Set internal osc. as PLL source
   SFRPAGE = LEGACY_PAGE;
   FLSCL = 0x10;                // Set FLASH read time for 50MHz clk or less
   SFRPAGE = CONFIG_PAGE;
   PLL0CN |= 0x01;              // Enable Power to PLL
   PLL0DIV = 0x01;              // Set Pre-divide value to N (N = 1)
   PLL0FLT = 0x01;              // Set the PLL filter register for
                                // a reference clock from 19 - 30 MHz
                                // and an output clock from 45 - 80 MHz
   PLL0MUL = 0x02;              // Multiply SYSCLK by M (M = 2)

   for (i = 0; i < 256; i++);   // Wait at least 5us
   PLL0CN |= 0x02;              // Enable the PLL
   // Wait until PLL frequency is locked
   for (i = 0 ; i<50000 && ((PLL0CN & 0x10) == 0) ; i++);
   CLKSEL = 0x02;               // Select PLL as SYSCLK source

   SFRPAGE = LEGACY_PAGE;

   EMI0CF = 0xD7;               // Split-Mode, non-multiplexed
                                // on P0-P3 (use 0xF7 for P4 - P7)

   EMI0TC = 0xB7;               // This value may be modified
                                // according to SYSCLK to meet the
                                // timing requirements for the CS8900A
                                // For example, EMI0TC should be >= 0xB7
                                // for a 100 MHz SYSCLK.

   /* init memory */
   RS485_ENABLE = 0;

   /* remove reset from CS8900A chip */
   delay_ms(10);
   CS8900A_RESET = 0;

   /* initialize UART0 */
   uart_init(0, BD_115200);
   PS0 = 1;                     // serial interrupt high priority

   /* reset system clock */
   sysclock_reset();

   /* Blink LEDs */
   led_blink(0, 3, 150);
   led_blink(1, 3, 150);
}

/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
   unsigned char i;

   if (init) {
      user_data.power     = 0;
      user_data.fans      = 100;
      user_data.hv_on     = 0;
      user_data.hv_demand = 0;
      for (i=0 ; i<18 ; i++)
         user_data.board_power[i] = 1;
      user_data.t_off     = 50;
   }

   user_data.error = 0;

   spi_init();
   led_clear();
   gpio_init();
   fan_init();

   monitor_init();
   monitor_set_alarm_limits(MON_TEMP, 0, user_data.t_off);
   monitor_set_alarm_limits(MON_T1,   0, user_data.t_off);
   monitor_set_alarm_limits(MON_T2,   0, user_data.t_off);
   monitor_set_alarm_limits(MON_T3,   0, user_data.t_off);

   menu_init();

   for (i=0 ; i<34 ; i++)
      user_write(i, NULL);
   update_data[35] = 0;
   user_update();

   spi_rx_wp = spi_rx_rp = 0;
   
   // initially allow writing
   _unlocked = 123;
   user_data.unlocked = 123;
}

/*---- User write function -----------------------------------------*/

void user_write(unsigned char index, unsigned char *buf) reentrant
{
   unsigned char n, ofs;

   if (index == 35) { // write data to SPI bus via spi_tx_buf
      n = buf[0] & 0x07;
      ofs = 2;

      if (n == 0x07) { // variable length
         n = buf[1];
         ofs = 3;
      }
      n--; // skip channel

      memcpy(spi_tx_buf, buf+ofs, n);
      spi_tx_n = n;
   }

   if (index == 37) {
      _unlocked = buf[2];
      user_data.unlocked = buf[2];
   }
   
   update_data[index] = 1;
}

/*---- User read function -----------------------------------------*/

unsigned char user_read(unsigned char index, unsigned char *buf) reentrant
{
   unsigned short i;

   if (index == 35) {
      for (i = 0 ; i<256 && spi_rx_wp != spi_rx_rp ; i++) {
         buf[i] = spi_rx_buf[spi_rx_rp];
         spi_rx_rp = (spi_rx_rp+1) % SPI_RX_SIZE;
      }

      return i;
   }

   return 0;
}

/*------------------------------------------------------------------*/

void user_update(void)
{
   unsigned char i;

   if (update_data[0]) {
      monitor_clear_alarms();
      if (uptime() >= 5)
         gpio_main_power(user_data.power);
      user_data.error = 0;
      start_time = time();
      update_data[0] = 0;
   }

   if (update_data[1]) {
      if (user_data.fans > 100)
         user_data.fans = 100;
      if (user_data.fans < 10)
         user_data.fans = 10;
      fan_set_speed(user_data.fans);
      update_data[1] = 0;
   }

   if (update_data[2]) {
      hv_power(user_data.hv_on);
      update_data[2] = 0;
   }

   if (update_data[9]) {
      hv_set_voltage(user_data.hv_demand);
      update_data[8] = 0;
   }

   if (update_data[16]) {
      monitor_set_alarm_limits(MON_TEMP, 0, user_data.t_off);
      monitor_set_alarm_limits(MON_T1,   0, user_data.t_off);
      monitor_set_alarm_limits(MON_T2,   0, user_data.t_off);
      monitor_set_alarm_limits(MON_T3,   0, user_data.t_off);
      monitor_clear_alarms();
   }

   for (i=0 ; i<18 ; i++) {
      if (update_data[17+i]) {
         gpio_board_power(user_data.board_power);
         if (user_data.board_power[i] == 0x03 || user_data.board_power[i] == 0x83) {
            /* issue init */
            gpio_board_select(i);
            gpio_board_init(1);
            gpio_board_init(0);
            gpio_board_select(0xFF);
            /* remove init bit */
            user_data.board_power[i] = (user_data.board_power[i] & ~0x02);
         }
         update_data[17+i] = 0;
      }
   }

   if (update_data[35]) {
      gpio_board_select(user_data.spi_slot);
      spi_adr(SPI_ADR_MISOMOSI, 0);

      for (i = 0; i < spi_tx_n; i++)
         spi_write_msb(spi_tx_buf[i]);

      spi_adr(0, 1);
      gpio_board_select(0xFF);
      update_data[35] = 0;
   }
}

/*------------------------------------------------------------------*/

void spi_poll()
{
   if (spi_rx_rp == spi_rx_wp) {
      spi_rx_buf[spi_rx_wp] = 'X';
      spi_rx_wp = (spi_rx_wp+1) % SPI_RX_SIZE;
   }
}

/*------------------------------------------------------------------*/

unsigned char board_present(unsigned char subadr)
{
   if (subadr > 17)
      return 0;
   return (user_data.board_power[subadr] & 0x80) > 0;
}

/*------------------------------------------------------------------*/

unsigned char flash_read_data(void)
{
   unsigned char code *p_flash;

   memset(&cli_cfg, 0, sizeof(cli_cfg));
   memset(&user_data, 0, sizeof(user_data));

   SFRPAGE = LEGACY_PAGE;
   p_flash = 0x80;          // second sector
   PSCTL = 0x04;            // select scratchpad area
   memcpy(&cli_cfg, p_flash, sizeof(cli_cfg));
   p_flash = 0x80 + sizeof(cli_cfg);
   memcpy(&user_data, p_flash, sizeof(user_data));
   PSCTL = 0x00;            // unselect scratchpad area

   if (cli_cfg.magic != 0x1234) {
      cli_cfg.node_addr = 0xFFFF;
      cli_cfg.group_addr = 0xFFFF;
      strcpy(cli_cfg.node_name, "CMB");
      cli_cfg.magic = 0x1234;
      return 1;
   }

   return 0;
}

/*------------------------------------------------------------------*/

void flash_write_client_data(void)
{
   // pointer has to reside in idata, otherwise it will conflict
   // with the flash access in XDATA
   unsigned char xdata * idata p_flash;

   /* Disable watchdog timer */
   watchdog_disable();

   DISABLE_INTERRUPTS;
   SFRPAGE = LEGACY_PAGE;

   /* erase scratchpad area */
   p_flash = 0x80;          // second sector
   FLSCL = FLSCL | 1;       // enable flash writes/erases
   PSCTL = 0x07;            // allow write and erase to scratchpad area
   *((char *)p_flash) = 0;  // erase page

   PSCTL = 0x05;            // allow write to scratchpad area

   p_flash = 0x80;
   memcpy(p_flash, &cli_cfg, sizeof(cli_cfg));
   p_flash = 0x80 + sizeof(cli_cfg);
   memcpy(p_flash, &user_data, sizeof(user_data)+4); // odd bytes will not be flashed

   FLSCL = FLSCL & ~1;      // disable flash writes/erases
   PSCTL = 0;               // disable flash access

   ENABLE_INTERRUPTS;

   watchdog_enable(10);
}

/*------------------------------------------------------------------*/

void measure()
{
   unsigned long t;
   unsigned char f, i;
   unsigned long d;
   static unsigned long last = 0;

   // only do ten times per second
   if (time() - last < 10)
      return;
   last = time();

   /*---- read plugin bits of all boards ----*/

   d = gpio_read_plugin();
   for (i=0 ; i<18 ; i++) {
      if (d & (1l<<i))
         user_data.board_power[i] |= 0x80;
      else
         user_data.board_power[i] &= ~0x80;
   }

   /*---- monitor voltage and currents ----*/

   user_data.voltage[0]   = monitor_read_adc(MON_VDD);
   user_data.voltage[1]   = monitor_read_adc(MON_5V);
   user_data.voltage[2]   = monitor_read_adc(MON_12V);
   user_data.voltage[3]   = monitor_read_adc(MON_24V);

   // current does not work below 0.4 A, so artificially set it to
   // zero if power is off
   if (user_data.power == 0)
      user_data.current = 0;
   else
      user_data.current = monitor_read_adc(MON_I);

   user_data.hv_measured  = hv_read_voltage();
   user_data.hv_current   = hv_read_current();

   /*---- read temperatures ----*/

   user_data.temp[0]      = monitor_read_adc(MON_TEMP);
   user_data.temp[1]      = monitor_read_adc(MON_T1);
   user_data.temp[2]      = monitor_read_adc(MON_T2);
   user_data.temp[3]      = monitor_read_adc(MON_T3);

   // set fans to 100% if temperature limit gets reached
   if (user_data.temp[0] >= user_data.t_off-1 ||
       user_data.temp[1] >= user_data.t_off-1 ||
       user_data.temp[2] >= user_data.t_off-1 ||
       user_data.temp[3] >= user_data.t_off-1) {
      user_data.fans = 100;
      update_data[1] = 1;
   }

   /*---- read alarm register of MAX1253 ----*/

   t = monitor_read_alarm_reg();
   if (t > 0 && time() > 300) {
      hv_power(0);
      gpio_main_power(0);

      if (t && (1l<<23))  // TEMP high
         user_data.error |= ERROR_T0;
      if (t && (1<< 9))  // AIN5 high
         user_data.error |= ERROR_T1;
      if (t && (1<< 7))  // AIN6 high
         user_data.error |= ERROR_T2;
      if (t && (1<< 5))  // AIN7 high
         user_data.error |= ERROR_T3;
   }

   /*---- read fan status starting 5 s after start ----*/

   if (user_data.power && time() > start_time + 500) {
      f = gpio_read_fan_fault();
      if (f & (1<<0))
         user_data.error |= ERROR_FAN1;
      if (f & (1<<1))
         user_data.error |= ERROR_FAN2;
      if (f & (1<<2))
         user_data.error |= ERROR_FAN3;
   }
}

/*------------------------------------------------------------------*/

void main(void)
{
   unsigned char i, start;

   SFRPAGE = 0;
   RSTSRC  = 0x02;               // Select VddMon as a reset source
   for (i=100 ; i>0 ; i--);      // Wait for VDD stabilized

   // initialize the C8051F12x
   setup();

   // turn off main power
   gpio_main_power(0);

   // get user data from flash
   flash_client = 0;
   flash_program = 0;
   i = flash_read_data();

   // initialize drivers
   user_init(i);

   // initialize submaster
   submaster_init();

   // turn on watchdog
   watchdog_enable(10);

   // remembr startup
   start = 1;

   do {
      if (!client_configured && !submaster_configured)
         led_blink(0, 1, 50);

      if (flash_client) {
         flash_client = 0;
         flash_write_client_data();
         client_configured = 1;

         flash_read_data();
      }

      if (flash_program) {
         flash_program = 0;

         led_puts("UPGRADE");

         /* wait until acknowledge has been sent */
         delay_ms(10);

         /* go to "bootloader" program */
         upgrade();
      }

      // turn on power earliest 5 seconds after power-up
      if (start && uptime() >= 5) {
         start = 0;
         update_data[0] = 1;
         user_update();
      }

      watchdog_refresh(0);

      submaster_yield();

      menu_yield();

      measure();

      user_update();

      spi_poll();

   } while (1);
}

