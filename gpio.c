/********************************************************************\

  Name:         gpio.c
  Created by:   Stefan Ritt

  Contents:     Driver for MCP23S17 General Purpose IO Chip
  
  

\********************************************************************/
      
#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "cmb.h"

// register addresses for non-banked layout (bank=0)
#define GPIO_IODIRA    0x00
#define GPIO_IODIRB    0x01
#define GPIO_IPOLA     0x02
#define GPIO_IPOLB     0x03
#define GPIO_GPINTENA  0x04
#define GPIO_GPINTENB  0x05
#define GPIO_DEFVALA   0x06
#define GPIO_DEFVALB   0x07
#define GPIO_INTCONA   0x08
#define GPIO_INTCONB   0x09
#define GPIO_IOCON     0x0A
#define GPIO_GPPUA     0x0C
#define GPIO_GPPUB     0x0D
#define GPIO_INTFA     0x0E
#define GPIO_INTFB     0x0F
#define GPIO_INTCAPA   0x10
#define GPIO_INTCAPB   0x11
#define GPIO_GPIOA     0x12
#define GPIO_GPIOB     0x13
#define GPIO_OLATA     0x14
#define GPIO_OLATB     0x15

// map physical slots WD0...WD7 DCB TCB WD8...WD15 to logical slots WD0..WD15 DCB TCB
static unsigned char code board_map[] = { 0,1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,8,9 };
static unsigned char gpio_mask[4][2] = { { 0xFF, 0xFF },
                                         { 0xFF, 0xFF },
                                         { 0xFF, 0xFF },
                                         { 0xFF, 0xFB } }; // POWER_ON = 0
   
/*------------------------------------------------------------------*/

void gpio_init()
{
   unsigned char i;
   
   gpio_write(0, GPIO_IOCON, 1<<3); // HAEN = 1, addresses all four devices

   // program all four devices as input with pull-up (can be used as open-drain output)
   for (i=0 ; i< 4 ; i++) {
      gpio_write(i, GPIO_IODIRA, gpio_mask[i][0]);
      gpio_write(i, GPIO_GPPUA,  0xFF);
      gpio_write(i, GPIO_IODIRB, gpio_mask[i][0]);
      gpio_write(i, GPIO_GPPUB,  0xFF);
   }
   
   // set /select lines to default state (high)
   gpio_board_select(0xFF);

   // set /flash_select and /init lines inactive (high)
   gpio_board_flash_select(0);
   gpio_board_init(0);
}

/*------------------------------------------------------------------*/

void gpio_write(unsigned char device, unsigned char adr, unsigned char d)
{
   spi_adr(SPI_ADR_SR, 0);
   // Device opcode 0x40 combined 3-bit hardware address, R/W = 0 (=write)
   spi_write_msb(0x40 | ((device & 0x07) << 1));
   spi_write_msb(adr);  
   spi_write_msb(d);  
   spi_adr(0, 1);
}

/*------------------------------------------------------------------*/

unsigned char gpio_read(unsigned char device, unsigned char adr)
{
   unsigned char d;
   
   spi_adr(SPI_ADR_SR, 0);
   // Device opcode 0x40 combined 3-bit hardware address, R/W = 1 (=read)
   spi_write_msb(0x40 | ((device & 0x07) << 1) | 0x01);
   spi_write_msb(adr);  
   d = spi_read_msb();  
   spi_adr(0, 1);
   return d;
}

/*------------------------------------------------------------------*/

void gpio_out(unsigned char device, unsigned char bit_no, unsigned char flag)
{
   unsigned char port;
   
   // write one bit to device

   if (bit_no < 8)
      port = 0;
   else {
      port = 1;
      bit_no -= 8;
   }
   
   if (flag) 
      gpio_mask[device][port] |= (1 << bit_no);
   else
      gpio_mask[device][port] &= ~(1 << bit_no);
   
   // switch all high outputs as input (= open drain), low outputs as push-pull
   gpio_write(device, port == 0 ? GPIO_IODIRA : GPIO_IODIRB, gpio_mask[device][port]);

   // set all low outputs low
   gpio_write(device, port == 0 ? GPIO_OLATA : GPIO_OLATB, gpio_mask[device][port]);
}

/*------------------------------------------------------------------*/

unsigned char gpio_in(unsigned char device, unsigned char port)
{
   // read port through GPIOA (port==0) or GPIOB (port==1)
   return gpio_read(device, port == 0 ? GPIO_GPIOA : GPIO_GPIOB);
}

/*------------------------------------------------------------------*/

unsigned char gpio_read_switches()
{
   unsigned char d;
   
   d = gpio_in(3, 0);
   return ~(d >> 3) & 0x0F;
}

/*------------------------------------------------------------------*/

unsigned long gpio_read_plugin()
{
   unsigned long d, ld;
   unsigned short i;

   // has to invert all bits since lines are active low   
   d = (~gpio_in(3, 0)) & 0x03;
   d = (d << 8) | (~gpio_in(1, 1) & 0xFF);
   d = (d << 8) | (~gpio_in(1, 0) & 0xFF);
   
   for (i=0,ld=0 ; i<18 ; i++)
      ld |= (d & (1l << board_map[i])) ? (1l << i) : 0;
   
   return ld;
}

/*------------------------------------------------------------------*/

unsigned char gpio_read_fan_fault()
{
   unsigned char pa, pb, d;
   
   pa = gpio_read(3, GPIO_GPIOA);
   pb = gpio_read(3, GPIO_GPIOB);
   d = 0;
   if ((pa & (1<<2)) == 0)
      d |= 1;
   if ((pa & (1<<7)) == 0)
      d |= 2;
   if ((pb & (1<<3)) == 0)
      d |= 4;
   
   return d;
}

/*------------------------------------------------------------------*/

void gpio_board_power(unsigned char p[18])
{
   unsigned char d;
   unsigned char lp[18];
   
   for (d=0 ; d<18 ; d++) {
      lp[board_map[d]] = 0xFF;
      
      if (p[d] == 0x81 || p[d] == 0x01)
         lp[board_map[d]] = 1;
      if (p[d] == 0x80 || p[d] == 0x00)
         lp[board_map[d]] = 0;
   }
   
   for (d=0 ; d<16 ; d++) {
      if (lp[d] == 0)
         gpio_out(2, d, 0); 
      if (lp[d] == 1)
         gpio_out(2, d, 1); 
   }
   
   if (lp[16] == 0)
      gpio_out(3, 14, 0); // PON_16
   if (lp[16] == 1)
      gpio_out(3, 14, 1); // PON_16
   
   if (lp[17] == 0)
      gpio_out(3, 15, 0); // PON_17
   if (lp[17] == 1)
      gpio_out(3, 15, 1); // PON_17
}

/*------------------------------------------------------------------*/

void gpio_board_select(unsigned char b)
{
   unsigned char d;

   if (b < sizeof(board_map))
      b = board_map[b];
   else
      b = 0xFF;

   for (d=0 ; d<16 ; d++)
      gpio_out(0, d, d != b); 

   gpio_out(3, 12, b != 16);
   gpio_out(3, 13, b != 17);
}

/*------------------------------------------------------------------*/

void gpio_board_flash_select(unsigned char flag)
{
   gpio_out(3, 9, !flag);
}

/*------------------------------------------------------------------*/

void gpio_board_init(unsigned char flag)
{
   gpio_out(3, 8, !flag);
}

/*------------------------------------------------------------------*/

void gpio_main_power(unsigned char flag)
{
   gpio_out(3, 10, flag);
}
