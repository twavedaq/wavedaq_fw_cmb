/********************************************************************\

  Name:         fan.c
  Created by:   Stefan Ritt

  Contents:     Driver for PWM fan control
  
  

\********************************************************************/
      
#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "cmb.h"

/*------------------------------------------------------------------*/

void fan_init()
{
   SFRPAGE = DAC0_PAGE;
   DAC0CN = 0x80; // turn on DAC0
   
   SFRPAGE = LEGACY_PAGE;
   REF0CN = 0x03; // enable voltage reference

   SFRPAGE = DAC0_PAGE;
   DAC0L = 0xFF;
   DAC0H = 0x0F;
}

/*------------------------------------------------------------------*/

void fan_set_speed(unsigned char value)
{
   unsigned short xdata d;
   
   if (value > 100)
      value = 100;
   
   d = (unsigned short)(value/100.0*4095);
   
   SFRPAGE = DAC0_PAGE;
   DAC0L = (d & 0xFF);
   DAC0H = (d >> 8) & 0x0F;
}
